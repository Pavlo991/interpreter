﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    internal class ClickStatement : Statement
    {
        private Expression text;

        public ClickStatement(Expression text)
        {
            this.text = text;
        }
        public string execute()
        {
            StringBuilder str = new StringBuilder();
            str.Append(text);
            return str.ToString();
            //return "";
        }

        public override string ToString()
        {
            return execute();
        }
    }
}