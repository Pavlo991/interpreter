﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class ConditionalExpression : Expression
    {
        public enum Operator
        {
            PLUS,
            MINUS,
            MULTIPLY,
            DIVIDE,

            EQUALS,
            NOT_EQUALS,

            LT,
            LTEQ,
            GT,
            GTEQ,

            AND,
            OR
        }

        private Operator operation;
        private Expression expr1, expr2;

        public ConditionalExpression(Operator operation, Expression expr1, Expression expr2)
        {
            this.operation = operation;
            this.expr1 = expr1;
            this.expr2 = expr2;
        }

        public Value eval()
        {
            Value value1 = expr1.eval();
            Value value2 = expr2.eval();

            double number1, number2;
            if (value1.GetType().Equals(typeof(StringValue)))
            {
                number1 = value1.asString().CompareTo(value2.asString());
                number2 = 0;
            }
            else
            {
                number1 = value1.asDouble();
                number2 = value2.asDouble();
            }
            switch (operation)
            {
                case Operator.LT: return new NumberValue(number1 < number2); 
                case Operator.LTEQ: return new NumberValue(number1 <= number2);
                case Operator.GT: return new NumberValue(number1 > number2);
                case Operator.GTEQ: return new NumberValue(number1 >= number2);
                case Operator.NOT_EQUALS: return new NumberValue(number1 != number2);
                case Operator.AND: return new NumberValue((number1 != 0) && (number2 != 0));
                case Operator.OR: return new NumberValue((number1 != 0) || (number2 != 0));
                case Operator.EQUALS:
                default:
                    return new NumberValue(number1 == number2);
            }
        }

        public override string ToString()
        {
            return String.Format("({0}{1}{2})", expr1, operation, expr2);
        }
    }
}
