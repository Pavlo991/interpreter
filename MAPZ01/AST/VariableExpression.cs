﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class VariableExpression: Expression
    {
        private string name;

        public VariableExpression(string name)
        {
            this.name = name;
        }

        public Value eval()
        {
                if (!Constants.isExists(name)) throw new NotImplementedException("The CONST doesn't exists");
                return Constants.get(name);
        }

        public override string ToString()
        {
            return String.Format("{0}",eval());
        }
    }
}
