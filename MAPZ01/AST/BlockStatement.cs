﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class BlockStatement: Statement
    {
        private List<Statement> statements;

        public BlockStatement()
        {
            statements = new List<Statement>();
        }

        public void add(Statement statement)
        {
            statements.Add(statement);
        }

        public string execute()
        {
            StringBuilder result = new StringBuilder();
            foreach (Statement statement in statements)
            {
                result.Append(statement.execute());
            }
            return result.ToString();
            //return "";
        }

        public List<Statement> getList()
        {
            return statements;
        }

        public override string ToString()
        {
            return execute();
        }
    }
}
