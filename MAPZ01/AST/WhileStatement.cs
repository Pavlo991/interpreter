﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class WhileStatement: Statement
    {
        private Expression condition;
        private Statement statement;

        private List<BlockStatement> listOfBlock;

        public WhileStatement(Expression condition, Statement statement)
        {
            this.condition = condition;
            this.statement = statement;
            listOfBlock = new List<BlockStatement>();
        }

        public string execute()
        {
            StringBuilder result = new StringBuilder();
            while (condition.eval().asDouble() != 0)
            {
                try
                {
                    //result.Append(statement.execute());
                    listOfBlock.Add(statement as BlockStatement);
                }
                catch (BreakStatement)
                {
                    break;
                }
                catch (ContinueStatement)
                {
                    continue; //необовязкове (ми все одно перейдемо)
                }
            }
            return result.ToString();
        }

        public List<Statement> getListOfStatement()
        {
            List<Statement> listOfStatement = new List<Statement>();
            foreach (BlockStatement blocks in listOfBlock)
            {
                foreach(Statement statament in blocks.getList())
                {
                    listOfStatement.Add(statament);
                }
            }
            return listOfStatement;
        }

        public override string ToString()
        {
            return "while " + condition + " " + statement;
        }
    }
}
