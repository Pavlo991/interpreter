﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class PrintStatement : Statement
    {
        public Expression expression;

        public PrintStatement(Expression expression)
        {
            this.expression = expression;
        }

        public string execute()
        {
           return expression.eval().asString();
        }

        public override string ToString()
        {
            return expression.ToString();
        }
    }
}