﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class ForStatement : Statement
    {
        private Statement initialization;
        private Expression termination; //умова
        private Statement increment; //інкремент
        private Statement block; //блок оператора
        private List<BlockStatement> listOfBlock;

        public ForStatement(Statement initialization, Expression termination, Statement increment, Statement block)
        {
            this.initialization = initialization;
            this.termination = termination;
            this.increment = increment;
            this.block = block;
            listOfBlock = new List<BlockStatement>();
        }

        public string execute()
        {
            StringBuilder result = new StringBuilder();
            for (initialization.execute(); termination.eval().asDouble() != 0; increment.execute())
            {
                try
                {
                    listOfBlock.Add(block as BlockStatement);
                    //result.Append(block.execute());
                }
                catch (BreakStatement)
                {
                    break;
                }
                catch (ContinueStatement)
                {
                    continue; //необовязкове (ми все одно перейдемо)
                }
            }
            return "";
            //return result.ToString();
        }

        public List<Statement> getListOfStatement()
        {
            List<Statement> listOfStatement = new List<Statement>();
            foreach (BlockStatement blocks in listOfBlock)
            {
                foreach (Statement statament in blocks.getList())
                {
                    listOfStatement.Add(statament);
                }
            }
            return listOfStatement;
        }

        public override string ToString()
        {
            return "for " + initialization + "; " + termination + "; " + increment + " " + block;
        }
    }
}
