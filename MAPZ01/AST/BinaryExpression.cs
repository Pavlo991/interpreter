﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class BinaryExpression: Expression
    {
        private char operation;
        private Expression expr1, expr2;

        public BinaryExpression(char operation, Expression expr1, Expression expr2)
        {
            this.operation = operation;
            this.expr1 = expr1;
            this.expr2 = expr2;
        }

        public Value eval()
        {
            {
                double number1 = expr1.eval().asDouble();
                double number2 = expr2.eval().asDouble();
                switch (operation)
                {
                    case '-': return new NumberValue(number1 - number2);
                    case '*': return new NumberValue(number1 * number2);
                    case '/': return new NumberValue(number1 / number2);
                    case '+':
                    default:
                        return new NumberValue(number1 + number2);
                }
            }
        }

        public override string ToString()
        {
            return String.Format("({0}{1}{2})",expr1,operation,expr2);
        }
    }
}
