﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    internal class FindStatement : Statement
    {
        private Expression idOfElem;

        public FindStatement(Expression idOfElem)
        {
            this.idOfElem = idOfElem;
        }

        public string execute()
        {
            StringBuilder str = new StringBuilder();
            str.Append(idOfElem);
            return str.ToString();
        }

        public override string ToString()
        {
            return execute();
        }
    }
}