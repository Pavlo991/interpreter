﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class BreakStatement : Exception, Statement
    {
        public string execute()
        {
            throw this;
        }

        public override string ToString()
        {
            return "break";
        }
    }
}
