﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class AssignmentStatement: Statement
    {
        public string variable;
        public Expression expression;

        public AssignmentStatement(string variable, Expression expression)
        {
            this.variable = variable;
            this.expression = expression;
            Constants.set(variable, expression.eval());
        }

        public string execute()
        {
            Value result = expression.eval();
            Constants.set(variable, result);
            return "";
        }

        public override string ToString()
        {
            return String.Format("{0} = {1}", variable, expression);
       }
    }
}
