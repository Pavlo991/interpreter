﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class Lexer
    {
        private static string OPERATOR_CHARS = "+-*/(){}=<>!&|;.";

        private static Dictionary<string, TokenType> OPERATORS = new Dictionary<string, TokenType>()
        {
            {"+", TokenType.PLUS },
            {"-", TokenType.MINUS },
            {"*", TokenType.MULTIPLICATION },
            {"/", TokenType.DIVISION },
            {"(", TokenType.LPAREN },
            {")", TokenType.RPAREN },
            {"{", TokenType.LBRACE },
            {"}", TokenType.RBRACE },
            {"=", TokenType.EQ },
            {"<", TokenType.LESS },
            {">", TokenType.GREATER },
            {";", TokenType.DOTECOMMA },
            {".", TokenType.DOT },

            {"!", TokenType.EXCL },
            {"&", TokenType.AMP },
            {"|", TokenType.BAR },

            {"==", TokenType.EQEQ },
            {"!=", TokenType.EXCLEQ },
            {"<=", TokenType.LESSEQ },
            {">=", TokenType.GREATEREQ },

            {"&&", TokenType.AMPAMP },
            {"||", TokenType.BARBAR }
        };
        

        private string input;
        private List<Token> tokens;
        private int pos;
        private int length;

        public Lexer(string input)
        {
            this.input = input;
            length = input.Length;
            tokens = new List<Token>();
        }

        public List<Token> tokenize()
        {
            while (pos < length)
            {
                char current = peek(0);
                if (Char.IsDigit(current)) tokenizeNumber();
                else if (Char.IsLetter(current)) tokenizeWord();
                else if (OPERATOR_CHARS.IndexOf(current) != -1)
                {
                    tokenizeOperator();
                }
                else if(current == '"')
                {
                    tokenizeText();
                }
                else next();
            }
            return tokens;
        }

        private void tokenizeWord()
        {
            StringBuilder buffer = new StringBuilder();
            char current = peek(0);
            while (true)
            {
                if (!Char.IsLetterOrDigit(current) && (current != '_') && (current != '$'))
                {
                    break;
                }
                buffer.Append(current);
                current = next();
            }
            string word = buffer.ToString();
            switch (word)
            {
                case "print":
                    addtoken(TokenType.PRINT); break;
                case "if": addtoken(TokenType.IF); break;
                case "else": addtoken(TokenType.ELSE); break;
                case "while": addtoken(TokenType.WHILE); break;
                case "for": addtoken(TokenType.FOR); break;
                case "do": addtoken(TokenType.DO); break;
                case "break": addtoken(TokenType.BRAKE); break;
                case "continue": addtoken(TokenType.CONTINUE); break;
                case "go": addtoken(TokenType.GO); break;
                case "find": addtoken(TokenType.FIND); break;
                case "set": addtoken(TokenType.SETINPUT); break;
                case "click": addtoken(TokenType.CLICK); break;
                case "quite": addtoken(TokenType.QUITE); break;
                default:
                    addtoken(TokenType.WORD, word); break;
            }
        }
        private void tokenizeText()
        {
            next(); //skip "
            StringBuilder buffer = new StringBuilder();
            char current = peek(0);
            while (true)
            {
                if (current == '\\')
                {
                    current = next();
                    switch (current)
                    {
                        case '"': current = next(); buffer.Append('"'); continue;
                        case 't': current = next(); buffer.Append('\t'); continue;
                        case 'n': current = next(); buffer.Append('\n'); continue;
                    }
                    buffer.Append('\\');
                    continue;
                }
                if (current == '"')
                {
                    break;
                }
                buffer.Append(current);
                current = next();
            }
            next(); //skip closing "
            addtoken(TokenType.TEXT, buffer.ToString());
        }

        private void tokenizeOperator()
        {
            char current = peek(0);
            StringBuilder buffer = new StringBuilder();
            while (true)
            {
                string text = buffer.ToString();
                if(!OPERATORS.ContainsKey(text + current) && !(text.Length == 0))
                {
                    addtoken(OPERATORS[text]);
                    return;
                }
                buffer.Append(current);
                current = next();
            }
        }

        private void tokenizeNumber()
        {
            StringBuilder buffer = new StringBuilder();
            char current = peek(0);
            while (true)
            {
                if (current == ',')
                {
                    if (buffer.ToString().IndexOf(",") != -1) throw new NotImplementedException("The dot(.) already use in number");
                }
                else if (!Char.IsDigit(current))
                {
                    break;
                }
                buffer.Append(current);
                current = next();
            }
            addtoken(TokenType.NUMBER, buffer.ToString());
        }

        private void addtoken(TokenType type)
        {
            addtoken(type, "");
        }
        private void addtoken(TokenType type, string text)
        {
            tokens.Add(new Token(type, text));
        }
        private char next()
        {
            pos++;
            return peek(0);
        }
        private char peek(int relativePos)
        {
            int position = pos + relativePos;
            if (position >= length) return '\0';
            return input.ElementAt(position);
        }
    }
}
