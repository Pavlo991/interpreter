﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class Parser
    {
        private static Token EOF = new Token(TokenType.EOF, "");
        private List<Token> tokens;
        private int pos;
        private int size;
        private List<Token> assignmentStatements;

        public Parser(List<Token> tokens)
        {
            this.tokens = tokens;
            size = tokens.Count;
            assignmentStatements = new List<Token>();
        }

        public List<Statement> parse()
        {
            List<Statement> result = new List<Statement>();
            while (!match(TokenType.EOF))
            {
                result.Add(statement());
            }
            return result;
        }

        private Statement block()
        {
            BlockStatement block = new BlockStatement();
            consume(TokenType.LBRACE);
            while (!match(TokenType.RBRACE))
            {
                block.add(statement());
            }
            return block;
        }

        private Statement statementOrBlock()
        {
            if (get(0).getType() == TokenType.LBRACE) return block();
            return statement();
        }

        private Statement statement()
        {
            if (match(TokenType.PRINT))
            {
                return new PrintStatement(expression());
            }
            if (match(TokenType.IF))
            {
                return ifElse();
            }
            if (match(TokenType.WHILE))
            {
                return whileStatement();
            }
            if (match(TokenType.BRAKE))
            {
                return new BreakStatement();
            }
            if (match(TokenType.CONTINUE))
            {
                return new ContinueStatement();
            }
            if (match(TokenType.DO))
            {
                return doWhileStatement();
            }
            if (match(TokenType.GO))
            {
                return goStatement();
            }
            if (match(TokenType.FIND))
            {
                return findStatement();
            }
            if (match(TokenType.SETINPUT))
            {
                return setInputStatement();
            }
            if (match(TokenType.CLICK))
            {
                return clickStatement();
            }
            if (match(TokenType.FOR))
            {
                return forStatement();
            }
            if (match(TokenType.QUITE))
            {
                return new QuiteStatement();
            }
            return assignmentStatement();
        }

        private Statement assignmentStatement()
        {
            Token current = get(0);
            if (match(TokenType.WORD) && get(0).getType() == TokenType.EQ)
            {
                string variable = current.getText();
                consume(TokenType.EQ);
                return new AssignmentStatement(variable, expression());
            }
            throw new NotImplementedException("Unknown statement");
        }
        private Statement ifElse()
        {
            Expression condition = expression();
            Statement ifStatement = statementOrBlock();
            Statement elseStatement;
            if (match(TokenType.ELSE))
            {
                elseStatement = statementOrBlock();
            }
            else
            {
                elseStatement = null;
            }
             return new IfStatement(condition, ifStatement, elseStatement);
        }

        private Statement whileStatement()
        {
            Expression condition = expression();
            Statement statement = statementOrBlock();
            return new WhileStatement(condition, statement);
        }
        private Statement goStatement()
        {
            Expression link = expression();
            return new GoStatement(link);
        }
        private Statement findStatement()
        {
            Expression idOfElem = expression();
            return new FindStatement(idOfElem);
        }
        private Statement setInputStatement()
        {
            Expression text = expression();
            return new SetInputStatement(text);
        }
        private Statement clickStatement()
        {
            Expression text = expression();
            return new ClickStatement(text);
        }
        private Statement doWhileStatement()
        {            
            Statement statement = statementOrBlock();
            consume(TokenType.WHILE);
            Expression condition = expression();
            return new DoWhileStatement(condition, statement);
        }

        private Statement forStatement()
        {
            Statement initialization = assignmentStatement();
            consume(TokenType.DOTECOMMA);
            Expression termination = expression();
            consume(TokenType.DOTECOMMA);
            Statement increment = assignmentStatement();
            Statement block = statementOrBlock();

            return new ForStatement(initialization, termination, increment, block);


        }
        //перевірка токена
        private Token consume(TokenType type)
        {
            Token current = get(0);
            if (type != current.getType()) throw new NotImplementedException("Token " + current + " does not match " + type);
            pos++;
            return current;
        }

        private Expression expression()
        {
            return logicalOR();
        }

        private Expression logicalOR()
        {
            Expression result = logicalAND();

            while (true)
            {
                if (match(TokenType.BARBAR))
                {
                    result =  new ConditionalExpression(ConditionalExpression.Operator.OR, result, logicalAND());
                    continue;
                }
                break;
            }
            return result;
        }
        private Expression logicalAND()
        {
            Expression result = equality();

            while (true)
            {
                if (match(TokenType.AMPAMP))
                {
                    result =  new ConditionalExpression(ConditionalExpression.Operator.AND, result, equality());
                    continue;
                }
                break;
            }
            return result;
        }
        private Expression equality()
        {
            Expression result = conditional();

            if (match(TokenType.EQEQ))
            {
                return new ConditionalExpression(ConditionalExpression.Operator.EQUALS, result, conditional());
            }
            if (match(TokenType.EXCLEQ))
            {
                return new ConditionalExpression(ConditionalExpression.Operator.NOT_EQUALS, result, conditional());
            }
            return result;
        }
        private Expression conditional()
        {
            Expression result = additive();
            while (true)
            {
                if (match(TokenType.LESS))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.LT, result, additive());
                    continue;
                }
                if (match(TokenType.LESSEQ))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.LTEQ, result, additive());
                    continue;
                }
                if (match(TokenType.GREATER))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.GT, result, additive());
                    continue;
                }
                if (match(TokenType.GREATEREQ))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.GTEQ, result, additive());
                    continue;
                }
                break;
            }
            return result;
        }
        private Expression additive()
        {
            Expression expr = multiplicative();
            while (true)
            {
                if (match(TokenType.PLUS))
                {
                    expr = new BinaryExpression('+', expr, multiplicative());
                    continue;
                }
                if (match(TokenType.MINUS))
                {
                    expr = new BinaryExpression('-', expr, multiplicative());
                    continue;
                }
                break;
            }
            return expr;
        }
        // * /
        private Expression multiplicative()
        {
            Expression expr = unary();
            while (true)
            {
                if (match(TokenType.MULTIPLICATION))
                {
                    expr = new BinaryExpression('*', expr, unary());
                    continue;
                }
                if (match(TokenType.DIVISION))
                {
                    expr = new BinaryExpression('/', expr, unary());
                    continue;
                }
                break;
            }

            return expr;
        }
        // + -
        private Expression unary()
        {
            if (match(TokenType.MINUS))
            {
                return new UnaryExpression('-', primary());
            }
            if (match(TokenType.PLUS))
            {
                return primary();
            }
            return primary();
        }
        //числа, стрічки
        private Expression primary()
        {
            Token current = get(0);
            if (match(TokenType.NUMBER))
            {
                return new ValueExpression(Double.Parse(current.getText()));
            }
            if (match(TokenType.WORD))
            {
                return new VariableExpression(current.getText());
            }
            if (match(TokenType.TEXT))
            {
                return new ValueExpression(current.getText());
            }
            if (match(TokenType.LPAREN))
            {
                Expression result = expression();
                match(TokenType.RPAREN);
                return result;
            }
            throw new NotImplementedException("Unknown expression");
        }

        private bool match(TokenType type)
        {
            Token current = get(0);
            if (type != current.getType()) return false;
            pos++;
            return true;
        }
        private Token get(int relativePos)
        {
            int position = pos + relativePos;
            if (position >= size) return EOF;
            return tokens[position];
        }
    }
}
