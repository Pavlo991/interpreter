﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace MAPZ01
{
    public partial class Form1 : Form
    {
        IWebDriver browser;
        IWebElement serchElem;
        List<TreeNode> tree;
        TreeNode tovarNode;
        List<Statement> statements;

        int indexOfNode = 0;
        public Form1()
        {
            InitializeComponent();
            tree = new List<TreeNode>();
            tovarNode = new TreeNode("Expressions");
            //tovarNode.Nodes.Add(new TreeNode("AsygStat"));
            treeView1.Nodes.Add(tovarNode);
        }

        //private void createTree(List<Statement> list, TreeNode treeNode)
        private void createTree(Statement statement, TreeNode treeNode, string str)
        {
            if (statement.GetType().Equals(typeof(PrintStatement)))
            {
                TreeNode myNode = new TreeNode("Print");
                PrintStatement stat = statement as PrintStatement;
                myNode.Nodes.Add(stat.expression.ToString());
                treeNode.Nodes.Add(myNode);
            }
            else if (statement.GetType().Equals(typeof(AssignmentStatement)))
            {
                TreeNode myNode = new TreeNode("AssignmentStatement");
                AssignmentStatement stat = statement as AssignmentStatement;
                myNode.Nodes.Add("=");
                myNode.Nodes.Add(stat.variable);
                myNode.Nodes.Add(stat.expression.ToString());
                treeNode.Nodes.Add(myNode);
            }
            else if (statement.GetType().Equals(typeof(QuiteStatement)))
            {
                TreeNode myNode = new TreeNode("QuiteStatement");
                QuiteStatement stat = statement as QuiteStatement;
                myNode.Nodes.Add("Quite");
                treeNode.Nodes.Add(myNode);
            }
            else if (statement.GetType().Equals(typeof(GoStatement)))
            {
                TreeNode myNode = new TreeNode("GoStatement");
                GoStatement stat = statement as GoStatement;
                myNode.Nodes.Add("Go");
                myNode.Nodes.Add(str);
                treeNode.Nodes.Add(myNode);
            }
            else if (statement.GetType().Equals(typeof(FindStatement)))
            {
                TreeNode myNode = new TreeNode("FindStatement");
                FindStatement stat = statement as FindStatement;
                myNode.Nodes.Add("Find");
                myNode.Nodes.Add(str);
                treeNode.Nodes.Add(myNode);
            }
            else if (statement.GetType().Equals(typeof(SetInputStatement)))
            {
                TreeNode myNode = new TreeNode("SetStatement");
                SetInputStatement stat = statement as SetInputStatement;
                myNode.Nodes.Add("Set");
                myNode.Nodes.Add(str);
                treeNode.Nodes.Add(myNode);
            }
            else if (statement.GetType().Equals(typeof(ClickStatement)))
            {
                TreeNode myNode = new TreeNode("ClickStatement");
                ClickStatement stat = statement as ClickStatement;
                myNode.Nodes.Add("Click");
                myNode.Nodes.Add(str);
                treeNode.Nodes.Add(myNode);
            }
            
        }

        private void AddStatements(Statement blocks, TreeNode node)
        {
            List<Statement> list = new List<Statement>();
            if (blocks.GetType().Equals(typeof(ForStatement)))
            {
                ForStatement block = blocks as ForStatement;
                block.execute();
                list = block.getListOfStatement();
            }
            else if (blocks.GetType().Equals(typeof(IfStatement)))
            {
                IfStatement block = blocks as IfStatement;
                block.execute();
                list = block.getListOfStatement();

            }
            else if (blocks.GetType().Equals(typeof(WhileStatement)))
            {
                WhileStatement block = blocks as WhileStatement;
                block.execute();
                list = block.getListOfStatement();
            }
            else if (blocks.GetType().Equals(typeof(DoWhileStatement)))
            {
                DoWhileStatement block = blocks as DoWhileStatement;
                block.execute();
                list = block.getListOfStatement();
            }

            if (list.Count > 0)
            {
                foreach (Statement statement in list)
                {
                    string statementExecuteString = "";
                    if (statement.GetType().Equals(typeof(GoStatement)))
                    {
                        browser = new ChromeDriver();
                        statementExecuteString = statement.execute();
                        browser.Navigate().GoToUrl(statementExecuteString);
                        createTree(statement, node, statementExecuteString);
                    }
                    else if (statement.GetType().Equals(typeof(FindStatement)))
                    {
                        statementExecuteString = statement.execute();
                        serchElem = browser.FindElement(By.Id(statementExecuteString));
                        createTree(statement, node, statementExecuteString);
                    }
                    else if (statement.GetType().Equals(typeof(SetInputStatement)))
                    {
                        statementExecuteString = statement.execute();
                        serchElem.SendKeys(statementExecuteString);
                        createTree(statement, node, statementExecuteString);
                    }
                    else if (statement.GetType().Equals(typeof(ClickStatement)))
                    {
                        statementExecuteString = statement.execute();
                        serchElem = browser.FindElement(By.Id(statementExecuteString));
                        serchElem.Click();
                        createTree(statement, node, statementExecuteString);
                    }
                    else if (statement.GetType().Equals(typeof(QuiteStatement)))
                    {
                        statementExecuteString = "";
                        System.Threading.Thread.Sleep(3000);
                        browser.Quit();
                        createTree(statement, node, statementExecuteString);
                    }
                    else if (statement.GetType().Equals(typeof(ForStatement)) || statement.GetType().Equals(typeof(WhileStatement))
                         || statement.GetType().Equals(typeof(IfStatement)) || statement.GetType().Equals(typeof(DoWhileStatement)))
                    {
                        TreeNode nodeVar = new TreeNode("BlockStatement");
                        node.Nodes.Add(nodeVar);
                        AddStatements(statement, nodeVar);
                    }
                    else if (statement.GetType().Equals(typeof(PrintStatement)))
                    {
                        statementExecuteString = statement.execute();
                        richTextBox2.AppendText(statementExecuteString);
                        createTree(statement, node, statementExecuteString);
                    }
                    else if (statement.GetType().Equals(typeof(AssignmentStatement)))
                    {
                        statementExecuteString = statement.execute();
                        richTextBox2.AppendText(statementExecuteString);
                        createTree(statement, node, statementExecuteString);
                    }
                    else statement.execute();
                }
            }
        }

        private void interpret_Click_1(object sender, EventArgs e)
        {
            richTextBox2.Clear();

            //--------------------------------------------------------
            string text = richTextBox1.Text;

            List<Token> tokens = new Lexer(text).tokenize();


            foreach (Token token in tokens)
            {
                Console.WriteLine(token);

            }
            statements = new Parser(tokens).parse();

            foreach (Statement statement in statements)
            {
                string statementExecuteString = "";
                if (statement.GetType().Equals(typeof(GoStatement)))
                {
                    browser = new ChromeDriver();
                    statementExecuteString = statement.execute();
                    browser.Navigate().GoToUrl(statementExecuteString);
                    createTree(statement, tovarNode, statementExecuteString);
                }
                else if (statement.GetType().Equals(typeof(FindStatement)))
                {
                    statementExecuteString = statement.execute();
                    serchElem = browser.FindElement(By.Id(statementExecuteString));
                    createTree(statement, tovarNode, statementExecuteString);
                }
                else if (statement.GetType().Equals(typeof(SetInputStatement)))
                {
                    statementExecuteString = statement.execute();
                    serchElem.SendKeys(statementExecuteString);
                    createTree(statement, tovarNode, statementExecuteString);
                }
                else if (statement.GetType().Equals(typeof(ClickStatement)))
                {
                    statementExecuteString = statement.execute();
                    serchElem = browser.FindElement(By.Id(statementExecuteString));
                    serchElem.Click();
                    createTree(statement, tovarNode, statementExecuteString);
                }
                else if (statement.GetType().Equals(typeof(QuiteStatement)))
                {
                    statementExecuteString = "";
                    System.Threading.Thread.Sleep(3000);
                    browser.Quit();
                    createTree(statement, tovarNode, statementExecuteString);
                }
                else if (statement.GetType().Equals(typeof(ForStatement)) || statement.GetType().Equals(typeof(WhileStatement))
                     || statement.GetType().Equals(typeof(IfStatement)) || statement.GetType().Equals(typeof(DoWhileStatement)))
                {
                    TreeNode node = new TreeNode("BlockStatement");
                    tovarNode.Nodes.Add(node);
                    AddStatements(statement, node);
                }
                else if (statement.GetType().Equals(typeof(PrintStatement)))
                {
                    statementExecuteString = statement.execute();
                    richTextBox2.AppendText(statementExecuteString);
                    createTree(statement, tovarNode, statementExecuteString);
                }
                else if (statement.GetType().Equals(typeof(AssignmentStatement)))
                {
                    statementExecuteString = statement.execute();
                    richTextBox2.AppendText(statementExecuteString);
                    createTree(statement, tovarNode, statementExecuteString);
                }
                else statement.execute();
            }


            foreach (Statement statement in statements)
            {
                Console.WriteLine(statement.ToString());
            }

        }
    }
}
