﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ01
{
    class NumberValue : Value
    {
        private double value;

        public NumberValue(double value)
        {
            this.value = value;
        }
        public NumberValue(bool value)
        {
            this.value = value ? 1 : 0;
        }
        double Value.asDouble()
        {
            return value;
        }

        string Value.asString()
        {
            return value.ToString();
        }
        public override string ToString()
        {
            return value.ToString();
        }
    }
}
