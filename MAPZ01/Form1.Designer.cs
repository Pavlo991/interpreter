﻿namespace MAPZ01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.interpret = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // interpret
            // 
            this.interpret.Location = new System.Drawing.Point(0, 592);
            this.interpret.Name = "interpret";
            this.interpret.Size = new System.Drawing.Size(263, 54);
            this.interpret.TabIndex = 10;
            this.interpret.Text = "Do it";
            this.interpret.UseVisualStyleBackColor = true;
            this.interpret.Click += new System.EventHandler(this.interpret_Click_1);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox1.Location = new System.Drawing.Point(0, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(263, 592);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox2.Location = new System.Drawing.Point(269, 3);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(275, 643);
            this.richTextBox2.TabIndex = 13;
            this.richTextBox2.Text = "";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(550, 3);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(222, 643);
            this.treeView1.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 646);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.interpret);
            this.Controls.Add(this.richTextBox1);
            this.MaximumSize = new System.Drawing.Size(800, 685);
            this.MinimumSize = new System.Drawing.Size(800, 685);
            this.Name = "Form1";
            this.Text = "Interpreter";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button interpret;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.TreeView treeView1;
    }
}

